const moduleA = {
    namespaced: true,
    state: {
        countA: 5
    },
    mutations: {
        incrementA (state, payload) {
            state.countA = payload + state.countA;
            console.log(state.countA);
        }
    },
    actions: {
        incrementA ({ state, commit, rootState}) {
            commit('incrementA', rootState.count)
        }
    }
}

export default moduleA;
