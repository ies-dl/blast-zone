import Vue from 'vue';
import Vuex from 'vuex';

import moduleA from './modules/ModuleA'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        moduleA
    },
    state: {
        count: 3,
        todos: [
            { id: 1, done: true },
            { id: 2, done: false }
        ]
    },
    mutations: {
        increment (state, payload) {
            state.count = state.count + 2;
        }
    },
    getters: {
        doneTodos: state => {
            return state.todos.filter(todo => todo.done).length
        },
        getTodoById: (state) => (id) => {
            return state.todos.find(todo => todo.id === id)
        }
    },
    actions: {
        increment (context) {
            context.commit('increment')
        },
        actionA ({commit, state}) {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    commit('increment');
                    console.log('inc');
                    resolve();
                }, 5000);
            })
        }
    }
})

export default store;
