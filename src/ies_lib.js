const $ = {

    /////////////////////////
    // General Functions/////
    /////////////////////////

    // Clones object without binding
    // Accepts object
    deepClone: (obj) => {
        return JSON.parse(JSON.stringify(obj))
    },

    // Convert string to lowercase
    // Accepts string
    toLower: (text) => {
        return text.toString().toLowerCase();
    },

    // Convert string to uppercase
    // Accepts string
    toUpper: (text) => {
        return text.toString().toUpperCase();
    },

    // Merge arrays of string with space in between
    // Accepts array of string
    mergeStrings: (array) => {
        var fullString = '';
        array.forEach((item) => {
            fullString = fullString + item + ' ';
        });
        return fullString.slice(0, fullString.length - 1);
    },

    // Search array and array objects for term
    // Accepts array and string
    searchArray: (entries, term) => {
        var items = $.deepClone(entries)
        if (term) {
            return items.filter((item) => {
                if (typeof item === 'object') {
                    var isMatch = false;
                    for (var propertyName in item) {
                        isMatch = $.toLower(item[propertyName]).includes($.toLower(term));
                    }
                    return isMatch;
                } else return $.toLower(item).includes($.toLower(term));
            });
        } else return items;
    },

    // Sort object array by property descending or ascending
    // Accepts object array, property name in string, and string of either 'asc' or 'desc'
    sortObject: (objAr, prop, text) => {
        return objAr.sort((a, b) => {
            if (text === 'asc') {
                if (a[prop] > b[prop]) return 1;
                else return -1;
            } else if (text === 'desc') {
                if (a[prop] < b[prop]) return 1;
                else return -1;
            }
        });
    },

    // Convert date format mm/dd/yyyy to yyyy-mm-dd
    // Accepts string
    convertDateA: (prop) => {
        const rPtrn = new RegExp(/(19|20)\d\d-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])\b/, 'g');
        const rPtrnOther = new RegExp(/(0?[1-9]|1[012]|[1-9])\/(0?[1-9]|[12][0-9]|3[01]|[1-9])\/(19|20)\d\d/, 'g');
        if (!rPtrn.test(prop)) {
            if (rPtrnOther.test(prop)) {
                var editDate = prop.split('/');
                if (editDate[0].length === 1) editDate[0] = '0' + editDate[0];
                if (editDate[1].length === 1) editDate[1] = '0' + editDate[1];
                prop = editDate[2] + '-' + editDate[0] + '-' + editDate[1];
            } else prop = new Date().toISOString().slice(0, 10);
        }
        return prop;
    },

    // Convert date format yyyy-mm-dd to mm/dd/yyyy
    // Accepts string
    convertDateB: (prop) => {
        const rPtrn = new RegExp(/(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01]|[1-9])\/(19|20)\d\d/, 'g');
        const rPtrnOther = new RegExp(/(19|20)\d\d-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])\b/, 'g');
        if (!rPtrn.test(prop)) {
            if (rPtrnOther.test(prop)) {
                var editDate = prop.split('-');
                if (editDate[0].length === 1) editDate[0] = '0' + editDate[0];
                if (editDate[1].length === 1) editDate[1] = '0' + editDate[1];
                prop = editDate[1] + '/' + editDate[2] + '/' + editDate[0];
            } else prop = new Date().toISOString().slice(0, 10);
        }
        return prop;
    },

    // Validate Email
    // Accepts string
    validateEmail: (email) => {
        const rPtrn = new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov)\b/, 'g');
        return rPtrn.test($.toLower(email));
    },

    // Validate Inputs - returns false if an element is null, undefined, empty string, empty array, or empty object
    // Accepts array
    validateInputs: (entries) => {
        var valid = true;
        entries.forEach((item) => {
            if (typeof item === 'object' && item !== null) valid = Object.keys(item).length === 0 ? false : valid;
            else if (typeof item === 'array') valid = item.length === 0 ? false : valid;
            else valid = !item && item !== 0 ? false : valid;
        });
        return valid;
    },


    ///////////////////////////
    // VUE Functions //////////
    // In this functions, 'this' is referenced in the parameter obj
    ///////////////////////////

    // Make an API request
    // Accepts an object in the following format:
    // {
    //    type: *API method,
    //    route: *API route,
    //    load: *API package
    // }
    apiRequest: async (obj, pack) => {
        var data;
        await obj.axios[pack.type](pack.route, pack.load, { withCredentials: true })
            .then((res) => {
                data = res.data;
            });
        return data;
    },


    // Get dimensions of scoped component
    // Works if function is called after component is mounted
    getDimensions: (obj) => {
        return {
            height: obj.$el.clientHeight,
            width: obj.$el.clientWidth
        }
    },

    // Reset values to either null, empty object, or empty array
    // Accepts array
    resetValues: (obj, entries) => {
        entries.forEach((item) => {
            if (obj[item] === null) return;
            else if (Array.isArray(obj[item])) obj[item] = [];
            else if (typeof obj[item] === 'object') obj[item] = {};
            else obj[item] = null;
        });
    },

    // Reset boolean values to false
    // Accepts array
    resetBooleans: (obj, entries) => {
        entries.forEach((item) => {
            obj[item] = false;
        });
    }


};

export default $;
